package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

type Airline struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type Route struct {
	ID          int64  `json:"id"`
	AirlineID   string `json:"airlines_id"`
	FlightCode  string `json:"flight_code"`
	Origin      string `json:"origin"`
	Destination string `json:"destination"`
}

type Schedule struct {
	ID       int64  `json:"id"`
	RouteID  string `json:"route_id"`
	Date     string `json:"date"`
	ToD      string `json:"time_of_departure"`
	Duration string `json:"duration"`
	Status   string `json:"status"`
}

func getAirlines(c *gin.Context) {
	var airlines []Airline

	rows, err := db.Query("SELECT * FROM airlines")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var arl Airline
		if err := rows.Scan(&arl.ID, &arl.Name); err != nil {
			log.Fatal(err)
		}
		airlines = append(airlines, arl)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	c.IndentedJSON(http.StatusOK, airlines)
}

func getRoutes(c *gin.Context) {
	var routes []Route

	rows, err := db.Query("SELECT * FROM route")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var rte Route
		if err := rows.Scan(&rte.ID, &rte.AirlineID, &rte.FlightCode, &rte.Origin, &rte.Destination); err != nil {
			log.Fatal(err)
		}
		routes = append(routes, rte)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	c.IndentedJSON(http.StatusOK, routes)
}

func getScheduleByDate(c *gin.Context) {
	var schDate Schedule
	var schedule []Schedule

	if err := c.BindJSON(&schDate); err != nil {
		return
	}

	rows, err := db.Query("SELECT * FROM schedule WHERE date = ?", schDate.Date)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var sch Schedule
		if err := rows.Scan(&sch.ID, &sch.RouteID, &sch.Date, &sch.ToD, &sch.Duration, &sch.Status); err != nil {
			log.Fatal(err)
		}
		schedule = append(schedule, sch)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	c.IndentedJSON(http.StatusOK, schedule)
}

func createAirline(c *gin.Context) {
	var arl Airline

	if err := c.BindJSON(&arl); err != nil {
		return
	}

	result, err := db.Exec("INSERT INTO airlines VALUES (DEFAULT, ?)", arl.Name)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}
	id, err := result.LastInsertId()
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}

	arl.ID = id
	c.IndentedJSON(http.StatusOK, arl)
}

func createRoute(c *gin.Context) {
	var rte Route

	if err := c.BindJSON(&rte); err != nil {
		return
	}

	result, err := db.Exec("INSERT INTO route (id, airline_id, flight_code, origin, destination) VALUES (DEFAULT, ?, ?, ?, ?)", rte.AirlineID, rte.FlightCode, rte.Origin, rte.Destination)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}
	id, err := result.LastInsertId()
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}

	rte.ID = id
	c.IndentedJSON(http.StatusOK, rte.ID)
}

func createSchedule(c *gin.Context) {
	var sch Schedule

	if err := c.BindJSON(&sch); err != nil {
		return
	}

	result, err := db.Exec("INSERT INTO schedule (id, route_id, date, tod, duration, status) VALUES (DEFAULT, ?, ?, ?, ?, ?)", sch.RouteID, sch.Date, sch.ToD, sch.Duration, sch.Status)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}
	id, err := result.LastInsertId()
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}

	sch.ID = id
	c.IndentedJSON(http.StatusOK, sch.ID)
}

func delaySchedule(c *gin.Context) {
	var schDelay Schedule

	if err := c.BindJSON(&schDelay); err != nil {
		return
	}

	_, err := db.Exec("UPDATE schedule s SET s.tod = (SELECT DATE_ADD(tod, INTERVAL ? MINUTE) FROM schedule WHERE id = ?), s.status = 'delayed' WHERE s.id = ?", schDelay.Duration, schDelay.ID, schDelay.ID)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}

	c.IndentedJSON(http.StatusOK, schDelay.ID)
}

func cancelSchedule(c *gin.Context) {
	var schCancel Schedule

	if err := c.BindJSON(&schCancel); err != nil {
		return
	}

	_, err := db.Exec("UPDATE schedule s SET schedule_status = 'canceled' where schedule_id = ?", schCancel.ID)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err})
	}

	c.IndentedJSON(http.StatusOK, schCancel.ID)
}

func getSchedule(c *gin.Context) {
	var schedule []Schedule

	rows, err := db.Query("SELECT * FROM schedule")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var sch Schedule
		if err := rows.Scan(&sch.ID, &sch.RouteID, &sch.Date, &sch.ToD, &sch.Duration, &sch.Status); err != nil {
			log.Fatal(err)
		}
		schedule = append(schedule, sch)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	c.IndentedJSON(http.StatusOK, schedule)
}

//The schedule will be automatically reloaded every 1 minute
func autoSchedule() {
	for {
		db.Query("UPDATE schedule SET status = 'boarding' WHERE DATE_SUB(tod, INTERVAL 15 MINUTE) < CURRENT_TIME")

		db.Query("UPDATE schedule SET status = 'en-route' WHERE tod < CURRENT_TIME AND ADDTIME(tod, duration) > CURRENT_TIME")

		db.Query("UPDATE schedule SET status = 'landed' WHERE ADDTIME(tod, duration) < CURRENT_TIME")

		time.Sleep(60 * time.Second)
	}
}

func main() {

	OpenDatabase()
	defer CloseDatabase()

	routes := gin.Default()

	routes.GET("/", func(c *gin.Context) {
		c.JSON(200, "Welcome to the airlines schedule")
	})

	defer db.Close()

	go autoSchedule()

	router := gin.Default()
	router.GET("/airlines", getAirlines)
	router.POST("/airline/create", createAirline)
	router.GET("/routes", getRoutes)
	router.POST("/route/create", createRoute)
	router.POST("/schedule/date", getScheduleByDate)
	router.POST("/schedule/create", createSchedule)
	router.POST("/schedule/delay", delaySchedule)
	router.POST("/schedule/cancel", cancelSchedule)
	router.GET("/schedule/auto", getSchedule)
	router.Run("localhost:8080")
}
