PGDMP     '    4                z            airlines-schedule    14.2    14.2 #               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16693    airlines-schedule    DATABASE     ^   CREATE DATABASE "airlines-schedule" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'C';
 #   DROP DATABASE "airlines-schedule";
                postgres    false            �            1259    16727    airlines    TABLE     [   CREATE TABLE public.airlines (
    id bigint NOT NULL,
    name character(255) NOT NULL
);
    DROP TABLE public.airlines;
       public         heap    postgres    false            �            1259    16726    airlines_id_seq    SEQUENCE     x   CREATE SEQUENCE public.airlines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.airlines_id_seq;
       public          postgres    false    210                       0    0    airlines_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.airlines_id_seq OWNED BY public.airlines.id;
          public          postgres    false    209            �            1259    16735    route    TABLE     �   CREATE TABLE public.route (
    id bigint NOT NULL,
    airlines_id bigint NOT NULL,
    flight_code character varying(255) NOT NULL,
    origin character(255) NOT NULL,
    destination character(255) NOT NULL
);
    DROP TABLE public.route;
       public         heap    postgres    false            �            1259    16734    route_airlines_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.route_airlines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.route_airlines_id_seq;
       public          postgres    false    213                       0    0    route_airlines_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.route_airlines_id_seq OWNED BY public.route.airlines_id;
          public          postgres    false    212            �            1259    16733    route_id_seq    SEQUENCE     u   CREATE SEQUENCE public.route_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.route_id_seq;
       public          postgres    false    213                       0    0    route_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.route_id_seq OWNED BY public.route.id;
          public          postgres    false    211            �            1259    16749    schedule    TABLE     �   CREATE TABLE public.schedule (
    id bigint NOT NULL,
    route_id bigint NOT NULL,
    date date NOT NULL,
    time_of_departure time with time zone NOT NULL,
    duration character(255) NOT NULL,
    status character(255) NOT NULL
);
    DROP TABLE public.schedule;
       public         heap    postgres    false            �            1259    16747    schedule_id_seq    SEQUENCE     x   CREATE SEQUENCE public.schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.schedule_id_seq;
       public          postgres    false    216                       0    0    schedule_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.schedule_id_seq OWNED BY public.schedule.id;
          public          postgres    false    214            �            1259    16748    schedule_route_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.schedule_route_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.schedule_route_id_seq;
       public          postgres    false    216                       0    0    schedule_route_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.schedule_route_id_seq OWNED BY public.schedule.route_id;
          public          postgres    false    215            r           2604    16730    airlines id    DEFAULT     j   ALTER TABLE ONLY public.airlines ALTER COLUMN id SET DEFAULT nextval('public.airlines_id_seq'::regclass);
 :   ALTER TABLE public.airlines ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    209    210            s           2604    16738    route id    DEFAULT     d   ALTER TABLE ONLY public.route ALTER COLUMN id SET DEFAULT nextval('public.route_id_seq'::regclass);
 7   ALTER TABLE public.route ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    213    213            t           2604    16739    route airlines_id    DEFAULT     v   ALTER TABLE ONLY public.route ALTER COLUMN airlines_id SET DEFAULT nextval('public.route_airlines_id_seq'::regclass);
 @   ALTER TABLE public.route ALTER COLUMN airlines_id DROP DEFAULT;
       public          postgres    false    213    212    213            u           2604    16752    schedule id    DEFAULT     j   ALTER TABLE ONLY public.schedule ALTER COLUMN id SET DEFAULT nextval('public.schedule_id_seq'::regclass);
 :   ALTER TABLE public.schedule ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    214    216            v           2604    16753    schedule route_id    DEFAULT     v   ALTER TABLE ONLY public.schedule ALTER COLUMN route_id SET DEFAULT nextval('public.schedule_route_id_seq'::regclass);
 @   ALTER TABLE public.schedule ALTER COLUMN route_id DROP DEFAULT;
       public          postgres    false    215    216    216                      0    16727    airlines 
   TABLE DATA           ,   COPY public.airlines (id, name) FROM stdin;
    public          postgres    false    210   �$                 0    16735    route 
   TABLE DATA           R   COPY public.route (id, airlines_id, flight_code, origin, destination) FROM stdin;
    public          postgres    false    213   S%                 0    16749    schedule 
   TABLE DATA           [   COPY public.schedule (id, route_id, date, time_of_departure, duration, status) FROM stdin;
    public          postgres    false    216   �%                  0    0    airlines_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.airlines_id_seq', 5, true);
          public          postgres    false    209                       0    0    route_airlines_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.route_airlines_id_seq', 4, true);
          public          postgres    false    212                       0    0    route_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.route_id_seq', 4, true);
          public          postgres    false    211                        0    0    schedule_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.schedule_id_seq', 5, true);
          public          postgres    false    214            !           0    0    schedule_route_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.schedule_route_id_seq', 5, true);
          public          postgres    false    215            x           2606    16732    airlines airlines_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.airlines
    ADD CONSTRAINT airlines_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.airlines DROP CONSTRAINT airlines_pkey;
       public            postgres    false    210            z           2606    16741    route route_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.route
    ADD CONSTRAINT route_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.route DROP CONSTRAINT route_pkey;
       public            postgres    false    213            |           2606    16755    schedule schedule_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.schedule DROP CONSTRAINT schedule_pkey;
       public            postgres    false    216            }           2606    16742    route airlines_id    FK CONSTRAINT     w   ALTER TABLE ONLY public.route
    ADD CONSTRAINT airlines_id FOREIGN KEY (airlines_id) REFERENCES public.airlines(id);
 ;   ALTER TABLE ONLY public.route DROP CONSTRAINT airlines_id;
       public          postgres    false    3448    210    213            ~           2606    16756    schedule route_id    FK CONSTRAINT     q   ALTER TABLE ONLY public.schedule
    ADD CONSTRAINT route_id FOREIGN KEY (route_id) REFERENCES public.route(id);
 ;   ALTER TABLE ONLY public.schedule DROP CONSTRAINT route_id;
       public          postgres    false    213    3450    216               ^   x�3�t*�,�,�Pp�,*O�,Va�ˈ�57�(�$u�y
��9K�Fj
 ��	gpf^zbA~Q*(r2�FXj�2����đ�kd����� ��V         �   x���=
�0����)r���?�K��L�B���8���-4�4�Z$����\X�j��Ka�-d�0��1���K��ݤ�8��&������ٟ��rz��}�6�,�v�r�z�:�m-��X�}�%���j$=wD�Jj         e   x�3�4�4202�50�50�44�20 "msN��\��
8����3RSJsR�-��8�I��4M�I�8C�`�ΔԜ��Ԕ�v�@�=... �Ǐ�     