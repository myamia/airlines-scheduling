package main

import (
	"database/sql"
	"fmt"
)

// adjust according your database configuration
const (
	host   = "localhost"
	port   = 5433 //default port is 5432
	user   = "postgres"
	dbname = "airlines-schedule" //insert your database name
)

// Global DB handles
var db *sql.DB
var err error

// OpenDatabase : opens connection to database
func OpenDatabase() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s "+
		"dbname=%s sslmode=disable",
		host, port, user, "password", dbname) //change "password" to your database password
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	//defer db.Close()
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	fmt.Println("Successfully connected to the database!!!")
}

// CloseDatabase : closes database
func CloseDatabase() {
	db.Close()
}
